﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Fitxers
{
    class Fitxers
    {
        static void Main()
        {
            int selectedOption;
            do
            {
                Console.WriteLine(Directory.GetCurrentDirectory());
                selectedOption = Menu(new string[] { " 1. Parell i Senar ", " 2. Inventario ", " 3. WordCount ",
                                                    " 4. ConfigFile Language ", " 5. FindPaths ", " 0. Exit "});
                switch (selectedOption)
                {
                    case 1:
                        ParellSenar();
                        break;
                    case 2:
                        Inventario();
                        break;
                    case 3:
                        WordCount();
                        break;
                    case 4:
                        ConfigFileLang();
                        break;
                    case 5:
                        FindPaths();
                        break;
                    case 0:
                        break;
                }
                Pause();
                Console.Clear();
            } while (selectedOption != 0);
            
        }

        // Mostra un menú en base a un array d'opcions
        static int Menu(string[] options)
        {
            Console.WriteLine();
            Color(ConsoleColor.White, ConsoleColor.DarkGray);
            foreach (string option in options) { Console.WriteLine(option); }
            Color();
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓ: ", 0, options.Length);
            return selectedOption;
        }
        
        // Demana una dada que obligatoriament ha d'estar dins d'un rang. Repeteix fins que es dona una dada adequada i la retorna.
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum > max || parsedNum < min)
                {
                    Console.WriteLine($"El número ha d'estar entre {min} i {max}!");
                }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }
        
        // Crea una pausa en el código
        static void Pause(string prompt = "\n[PRESS ENTER TO CONTINUE]\n")
        {
            Color(ConsoleColor.Black, ConsoleColor.Gray);
            Console.Write(prompt);
            Color();
            Console.ReadLine();
        }
        
        // Cambia el color de la consola
        static void Color(ConsoleColor fg = ConsoleColor.White, ConsoleColor bg = ConsoleColor.Black)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
        }
        
        // Mostra un fitxer
        static void ShowFile(string path)
        {
            string fileName = GetFileName(path);
            
            Console.WriteLine(fileName);
            for (int i = 0; i < fileName.Length; i++) { Console.Write("="); }
            Console.WriteLine();

            if (File.ReadAllLines(path).Length != 0)
            {
                foreach (string line in File.ReadLines(path)){ Console.WriteLine(line); }
            }
            else { Console.WriteLine(); }

            for (int i = 0; i < fileName.Length; i++) { Console.Write("="); }
            Console.WriteLine();
        }
        
        // Returna el nom d'un fitxer, ignorant la resta del path
        static string GetFileName(string path)
        {
            if (path.LastIndexOf('/') > 0) { return path.Substring(path.LastIndexOf('/') + 1); }
            else { return path.Substring(path.LastIndexOf('\\') + 1); }
        }
        
        // Comprova si un número enter es parell o no
        static bool IsParell(int n)
        {
            if (n % 2 == 0) { return true; }
            return false;
        }
        // 
        static void FindFile(string path, string fileName)
        {
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(path));
            string[] files = Directory.GetFiles(path);
            foreach (string dir in dirs)
            {
                FindFile(dir, fileName);
            }
            foreach (string file in files)
            {
                if (file.Substring(file.Replace(@"\", @"/").LastIndexOf('/') + 1) == fileName)
                {
                    Console.WriteLine($"S'ha trobat el fitxer a:\n{Path.GetFullPath(file)}");
                }
            }

        }

        /*------ M E T O D O S -------------------------------------------*/
        static void ParellSenar()
        {
            string pathNum = @"..\..\..\..\Files\Numbers.txt";
            string pathOdd = @"..\..\..\..\Files\senars.txt";
            string pathEven = @"..\..\..\..\Files\parell.txt";

            FileStream f1 = File.Create(pathOdd);
            f1.Close();
            ShowFile(pathOdd);
            FileStream f2 = File.Create(pathEven);
            f2.Close();
            ShowFile(pathEven);

            Pause("\n[FITXERS BUITS]\n[ENTER PER CONTINUAR]\n");

            StreamWriter odd = File.AppendText(pathOdd);
            StreamWriter even = File.AppendText(pathEven);

            foreach (string line in File.ReadLines(pathNum))
            {
                if (IsParell(int.Parse(line)))
                {
                    even.WriteLine(line);
                }
                else { odd.WriteLine(line); }
                
            }
            odd.Close();
            even.Close();
            ShowFile(pathOdd);
            ShowFile(pathEven);
        }
        static void Inventario()
        {
            string path = @"..\..\..\..\Files\inventary.txt";
            if (!File.Exists(path))
            {
                File.Create(path).Close();
                
            }
            StreamWriter sw = File.AppendText(path);
            Console.Write("> Introduce el nombre: ");
            sw.Write($"{Console.ReadLine()},");
            Console.Write("> Introduce el modelo: ");
            sw.Write($"{Console.ReadLine()},");
            Console.Write("> Introduce el MAC: ");
            sw.Write($"{Console.ReadLine()},");
            Console.Write("> Introduce el año de fabricación: ");
            sw.Write($"{Console.ReadLine()}\n");
            sw.Close();
        }
        static void WordCount()
        {
            string path = @"..\..\..\..\Files\wordcount.txt";
            string[] txt = File.ReadAllText(path).Replace(",", "").Replace(".", "").Split(' ');
            string[] search;
            do
            {
                Console.Write("\n> ");
                search = Console.ReadLine().Split(' ');
                if (search[0] != "END")
                {
                    foreach (string s in search)
                    {
                        int count = 0;
                        foreach (string word in txt)
                        {
                            if (s.ToLower() == word.ToLower()) { count++; }
                            /* En el exemple del enunciat diu que quis apareix 5 vegades, això en la meva
                            opinió es un error, ja que després de comprovar el text probablement compta
                            també compta la paraula Quisque. El meu codi con compta això com una instancia
                            de 'quis' perque separa les paraule en base als seus espais.*/
                        }
                        Console.WriteLine($"\n{s}: {count}");
                    } 
                }
            } while (search[0] != "END");
        }
        static void ConfigFileLang()
        {
            string confPath = @"..\..\..\..\Files\config.txt";
            string langPath = @"..\..\..\..\Files\lang\";
            string[] config = File.ReadAllText(confPath).Replace("\r", "").Split('\n');
            string name = config[0].Substring(config[0].IndexOf(':') + 1);
            string lang = config[1].Substring(config[1].IndexOf(':') + 1);
            Console.WriteLine($"{name} {lang}");
            Console.Write("\n> ");
            string userCommand = Console.ReadLine();
            if (userCommand.Substring(0, userCommand.IndexOf(' ')) == "Idioma")
            {
                if (userCommand.Substring(userCommand.IndexOf(' ') + 1) == "es" || userCommand.Substring(userCommand.IndexOf(' ') + 1) == "cat")
                {
                    lang = userCommand.Substring(userCommand.IndexOf(' ') + 1);
                }
            }
            else if (userCommand.Substring(0, userCommand.IndexOf(' ')) == "Nombre")
            {
                name = userCommand.Substring(userCommand.IndexOf(' ') + 1);
            }
            Console.WriteLine($"{File.ReadAllText($"{langPath}lng_{lang}.txt")}{name}");
        }
        static void FindPaths()
        {
            Console.Write("> Introdueix el nom del fitxer a cercar: ");
            string fileName = Console.ReadLine();
            string path;
            do
            {
                Console.Write("> Introdueix el path en el que cercar: ");
                path = Console.ReadLine();
            } while (!Directory.Exists(path));
            FindFile(@path, fileName);
        }
        /*----------------------------------------------------------------*/
    }
}
